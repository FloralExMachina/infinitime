package blefs

import (
	"io/fs"
	"time"
)

// FileInfo implements fs.FileInfo
type FileInfo struct {
	name    string
	size    uint32
	modtime uint64
	mode    fs.FileMode
	isDir   bool
}

// Name returns the base name of the file
func (fi FileInfo) Name() string {
	return fi.name
}

// Size returns the total size of the file
func (fi FileInfo) Size() int64 {
	return int64(fi.size)
}

// Mode returns the mode of the file
func (fi FileInfo) Mode() fs.FileMode {
	return fi.mode
}

// ModTime returns the modification time of the file
// As of now, this is unimplemented in InfiniTime, and
// will always return 0.
func (fi FileInfo) ModTime() time.Time {
	return time.Unix(0, int64(fi.modtime))
}

// IsDir returns whether the file is a directory
func (fi FileInfo) IsDir() bool {
	return fi.isDir
}

// Sys is unimplemented and returns nil
func (fi FileInfo) Sys() interface{} {
	return nil
}
