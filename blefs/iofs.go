package blefs

import "io/fs"

type goFS struct {
	*FS
}

func (iofs goFS) Open(path string) (fs.File, error) {
	return iofs.FS.Open(path)
}

func (blefs *FS) GoFS() fs.FS {
	return goFS{blefs}
}
